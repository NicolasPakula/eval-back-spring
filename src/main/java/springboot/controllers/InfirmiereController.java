package springboot.controllers;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import springboot.models.Deplacement;
import springboot.models.Infirmiere;
import springboot.models.Patient;
import springboot.services.InfirmiereService;

@RestController
@CrossOrigin
@RequestMapping("infirmieres")
public class InfirmiereController {
	private static  Logger LOGGER = LoggerFactory.getLogger(InfirmiereController.class);
	
	@Autowired
	private InfirmiereService infirmiereService;
	
	@GetMapping
	public List<Infirmiere> findAll(){
		LOGGER.info("GET /infirmieres");
		return infirmiereService.findAll();
	}
	
	@GetMapping("{id}")
	public Infirmiere findById(@PathVariable Integer id){
		LOGGER.info("GET /infirmieres/"+id);
		return infirmiereService.findById(id);
	}
	
	@GetMapping("{id}/patients")
	public List<Patient> patientById(@PathVariable Integer id){
		LOGGER.info("GET /infirmieres/"+id+"/patients");
		return infirmiereService.patientById(id);
	}
	
	@GetMapping("{id}/deplacements")
	public List<Deplacement> deplacementsById(@PathVariable Integer id){
		LOGGER.info("GET /infirmieres/"+id+"/deplacements");
		return infirmiereService.deplacementsById(id);
	}
	
	@PostMapping
	public Infirmiere post(@RequestBody Infirmiere infirmiere) {
		LOGGER.info("POST /infirmieres");
		return infirmiereService.post(infirmiere);
	}
	
	@PutMapping
	public Infirmiere put(@RequestBody Infirmiere infirmiere) {
		LOGGER.info("PUT /infirmieres");
		return infirmiereService.put(infirmiere);
	}
	
	@DeleteMapping("{id}")
	public void delete(@PathVariable Integer id){
		LOGGER.info("DELETE /infirmieres/"+id);
		infirmiereService.delete(id);
	}
}
