package springboot.controllers;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import springboot.models.Deplacement;
import springboot.services.DeplacementService;

@RestController
@CrossOrigin
@RequestMapping("deplacements")
public class DeplacementController {
	private static  Logger LOGGER = LoggerFactory.getLogger(DeplacementController.class);
	@Autowired
	private DeplacementService deplacementService;
	
	@GetMapping
	public List<Deplacement> findAll(){
		LOGGER.info("GET /deplacements");
		return deplacementService.findAll();
	}
	
	@GetMapping("{id}")
	public Deplacement findById(@PathVariable Integer id){
		LOGGER.info("GET /deplacements/"+id);
		return deplacementService.findById(id);
	}
	
	@PostMapping
	public Deplacement post(@RequestBody Deplacement deplacement) {
		LOGGER.info("POST /deplacements");
		return deplacementService.post(deplacement);
	}
	
	@PutMapping
	public Deplacement put(@RequestBody Deplacement deplacement) {
		LOGGER.info("PUT /deplacements");
		return deplacementService.put(deplacement);
	}
	
	@DeleteMapping("{id}")
	public void delete(@PathVariable Integer id){
		LOGGER.info("DELETE /deplacements/"+id);
		deplacementService.delete(id);
	}
}
