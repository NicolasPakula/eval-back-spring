package springboot.controllers;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import springboot.models.Deplacement;
import springboot.models.Patient;
import springboot.services.PatientService;

@RestController
@CrossOrigin
@RequestMapping("patients")
public class PatientController {
	private static  Logger LOGGER = LoggerFactory.getLogger(PatientController.class);
	@Autowired
	private PatientService patientService;
	
	@GetMapping
	public List<Patient> findAll(){
		LOGGER.info("GET /patients");
		return patientService.findAll();
	}
	
	@GetMapping("{id}")
	public Patient findById(@PathVariable Integer id){
		LOGGER.info("GET /patients/"+id);
		return patientService.findById(id);
	}
	
	@GetMapping("{id}/deplacements")
	public List<Deplacement> deplacementsById(@PathVariable Integer id){
		LOGGER.info("GET /patients/"+id+"/deplacements");
		return patientService.deplacementsById(id);
	}
	
	@PostMapping
	public Patient post(@RequestBody Patient patient) {
		LOGGER.info("POST /patients");
		return patientService.post(patient);
	}
	
	@PutMapping
	public Patient put(@RequestBody Patient patient) {
		LOGGER.info("PUT /patients");
		return patientService.put(patient);
	}
	
	@DeleteMapping("{id}")
	public void delete(@PathVariable Integer id){
		LOGGER.info("DELETE /patients/"+id);
		patientService.delete(id);
	}
	
}
