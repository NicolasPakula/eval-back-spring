package springboot.controllers;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import springboot.models.Adresse;
import springboot.services.AdresseService;

@RestController
@CrossOrigin
@RequestMapping("adresses")
public class AdresseController {
	private static  Logger LOGGER = LoggerFactory.getLogger(AdresseController.class);
	@Autowired
	private AdresseService adresseService;
	
	@GetMapping
	public List<Adresse> findAll(){
		LOGGER.info("GET /adresses");
		return adresseService.findAll();
	}
	
	@GetMapping("{id}")
	public Adresse findById(@PathVariable Integer id){
		LOGGER.info("GET /adresses/"+id);
		return adresseService.findById(id);
	}
	
	@PostMapping
	public Adresse post(@RequestBody Adresse adresse) {
		LOGGER.info("POST /adresses");
		return adresseService.post(adresse);
	}
	
	@PutMapping
	public Adresse put(@RequestBody Adresse adresse) {
		LOGGER.info("PUT /adresses");
		return adresseService.put(adresse);
	}
	
	@DeleteMapping("{id}")
	public void delete(@PathVariable Integer id){
		LOGGER.info("DELETE /adresses/"+id);
		adresseService.delete(id);
	}
	
}
