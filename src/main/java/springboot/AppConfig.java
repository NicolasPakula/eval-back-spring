package springboot;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springboot.repositories.AdresseRepository;
import springboot.repositories.DeplacementRepository;
import springboot.repositories.InfirmiereRepository;
import springboot.repositories.PatientRepository;
import springboot.services.AdresseService;
import springboot.services.AdresseServiceImpl;
import springboot.services.DeplacementService;
import springboot.services.DeplacementServiceImpl;
import springboot.services.InfirmiereService;
import springboot.services.InfirmiereServiceImpl;
import springboot.services.PatientService;
import springboot.services.PatientServiceImpl;

@Configuration
public class AppConfig {
	@Bean
	public InfirmiereService infirmiereService(InfirmiereRepository infirmiereRepository,AdresseService adresseService,PatientRepository patientRepository,DeplacementRepository deplacementRepository) {
		return new InfirmiereServiceImpl(infirmiereRepository,adresseService,patientRepository,deplacementRepository);
	}
	
	@Bean
	public AdresseService adresseService(AdresseRepository adresseRepository) {
		return new AdresseServiceImpl(adresseRepository);
	}
	
	@Bean
	public PatientService patientService(PatientRepository patientRepository,AdresseService adresseService,DeplacementRepository deplacementRepository) {
		return new PatientServiceImpl(patientRepository, adresseService,deplacementRepository);
	}
	
	@Bean
	public DeplacementService deplacementService(DeplacementRepository deplacementRepository) {
		return new DeplacementServiceImpl(deplacementRepository);
	}
}
