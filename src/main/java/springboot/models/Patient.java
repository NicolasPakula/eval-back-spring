package springboot.models;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Patient {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	private String nom;
	private String prenom;
	@Column(name="date_de_naissance")
	private LocalDate dateDeNaissance;
	
	@Enumerated(EnumType.STRING)
	@Column(columnDefinition = "ENUM('Homme', 'Femme', 'Autre')")
	private Sexe sexe;
	@Column(name="numero_securite_sociale")
	private Long numeroSecuriteSociale;
	
	@ManyToOne
	@JoinColumn(name="infirmiere_id")
	private Infirmiere infirmiere;
	
	@ManyToOne
	@JoinColumn(name="adresse_id")
	private Adresse adresse;
}
