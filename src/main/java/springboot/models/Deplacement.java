package springboot.models;

import java.time.LocalDateTime;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Deplacement {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private Double cout;
	private LocalDateTime date;
	
	@ManyToOne
	@JoinColumn(name="infirmiere_id")
	private Infirmiere infirmiere;
	
	@ManyToOne
	@JoinColumn(name="patient_id")
	private Patient patient;
}
