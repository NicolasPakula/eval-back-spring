package springboot.models;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name="infirmiere")
public class Infirmiere {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@ManyToOne
	@JoinColumn(name="adresse_id")
	private Adresse adresse;
	@Column(name="numero_professionnel")
	private Integer numeroProfessionnel;
	private String nom;
	private String prenom;
	@Column(name="tel_pro")
	private String telPro;
	@Column(name="tel_perso")
	private String telPerso;
	

	
	@OneToMany(mappedBy="infirmiere")
	@JsonIgnore
	private List<Patient> patients;
}
