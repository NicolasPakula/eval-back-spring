package springboot.models;

public enum Sexe {
    Homme,
    Femme,
    Autre;
}
