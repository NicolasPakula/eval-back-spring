package springboot.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import springboot.models.Deplacement;

public interface DeplacementRepository extends JpaRepository<Deplacement, Integer>{

	public List<Deplacement> findByInfirmiereId(Integer id);

	public List<Deplacement> findByPatientId(Integer id);

}
