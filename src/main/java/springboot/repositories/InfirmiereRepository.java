package springboot.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import springboot.models.Infirmiere;

public interface InfirmiereRepository extends JpaRepository<Infirmiere, Integer>{

}
