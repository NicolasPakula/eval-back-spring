package springboot.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import springboot.models.Patient;

public interface PatientRepository extends JpaRepository<Patient, Integer>{

	public List<Patient> findByInfirmiereId(Integer id);

}
