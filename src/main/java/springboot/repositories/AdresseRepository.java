package springboot.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import springboot.models.Adresse;

public interface AdresseRepository extends JpaRepository<Adresse, Integer>{

	public Adresse findByNumeroAndRueAndCpAndVille(String numero, String rue, String cp, String ville);

}
