package springboot.services;

import java.util.List;

import springboot.models.Adresse;
import springboot.repositories.AdresseRepository;

public class AdresseServiceImpl implements AdresseService{
	
	private AdresseRepository adresseRepository;
	
	public AdresseServiceImpl(AdresseRepository adresseRepository) {
		this.adresseRepository = adresseRepository;
	}

	@Override
	public List<Adresse> findAll() {
		return adresseRepository.findAll();
	}

	@Override
	public Adresse findById(Integer id) {
		return adresseRepository.findById(id).orElse(null);
	}

	@Override
	public Adresse post(Adresse adresse) {
		return adresseRepository.save(adresse);
	}

	@Override
	public Adresse put(Adresse adresse) {
		return adresseRepository.save(adresse);
	}

	@Override
	public void delete(Integer id) {
		adresseRepository.deleteById(id);
	}

	@Override
	public Adresse insertWithExistCheck(Adresse adresse,String method) {
		Adresse check = adresseRepository.findByNumeroAndRueAndCpAndVille(adresse.getNumero(),adresse.getRue(),adresse.getCp(),adresse.getVille());
		if(check!=null) {
			return check;
		}
		else {
			if(method.equals("post")) {
				return post(adresse);
			}
			else {
				return put(adresse);
			}
		}
	}

}
