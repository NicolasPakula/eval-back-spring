package springboot.services;

import java.util.List;

import springboot.models.Deplacement;

public interface DeplacementService {

	List<Deplacement> findAll();

	Deplacement findById(Integer id);

	Deplacement post(Deplacement deplacement);

	Deplacement put(Deplacement deplacement);

	void delete(Integer id);

}
