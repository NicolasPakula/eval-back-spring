package springboot.services;

import java.util.List;

import springboot.models.Deplacement;
import springboot.models.Infirmiere;
import springboot.models.Patient;
import springboot.repositories.DeplacementRepository;
import springboot.repositories.InfirmiereRepository;
import springboot.repositories.PatientRepository;

public class InfirmiereServiceImpl implements InfirmiereService{
	
	private InfirmiereRepository infirmiereRepository;
	private AdresseService adresseService;
	private PatientRepository patientRepository;
	private DeplacementRepository deplacementRepository;
	
	public InfirmiereServiceImpl(InfirmiereRepository infirmiereRepository,AdresseService adresseService,PatientRepository patientRepository,DeplacementRepository deplacementRepository) {
		this.infirmiereRepository = infirmiereRepository;
		this.adresseService = adresseService;
		this.patientRepository = patientRepository;
		this.deplacementRepository = deplacementRepository;
	}

	@Override
	public List<Infirmiere> findAll() {
		return infirmiereRepository.findAll();
	}

	@Override
	public Infirmiere findById(Integer id) {
		return infirmiereRepository.findById(id).orElse(null);
	}

	@Override
	public Infirmiere post(Infirmiere infirmiere) {
		infirmiere.setAdresse(adresseService.insertWithExistCheck(infirmiere.getAdresse(), "post"));
		return infirmiereRepository.save(infirmiere);
	}

	@Override
	public Infirmiere put(Infirmiere infirmiere) {
		infirmiere.setAdresse(adresseService.insertWithExistCheck(infirmiere.getAdresse(), "put"));
		return infirmiereRepository.save(infirmiere);
	}

	@Override
	public void delete(Integer id) {
		infirmiereRepository.deleteById(id);
	}

	@Override
	public List<Patient> patientById(Integer id) {
		return patientRepository.findByInfirmiereId(id);
	}

	@Override
	public List<Deplacement> deplacementsById(Integer id) {
		return deplacementRepository.findByInfirmiereId(id);
	}
	
}
