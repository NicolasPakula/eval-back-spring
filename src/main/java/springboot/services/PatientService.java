package springboot.services;

import java.util.List;

import springboot.models.Deplacement;
import springboot.models.Patient;

public interface PatientService {

	public List<Patient> findAll();

	public Patient findById(Integer id);

	public Patient post(Patient patient);

	public Patient put(Patient patient);

	public void delete(Integer id);

	public List<Deplacement> deplacementsById(Integer id);

}
