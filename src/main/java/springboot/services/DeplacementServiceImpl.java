package springboot.services;

import java.util.List;

import springboot.models.Deplacement;
import springboot.repositories.DeplacementRepository;

public class DeplacementServiceImpl implements DeplacementService{
	
	private DeplacementRepository deplacementRepository;
	
	public DeplacementServiceImpl(DeplacementRepository deplacementRepository) {
		this.deplacementRepository = deplacementRepository;
	}

	@Override
	public List<Deplacement> findAll() {
		return deplacementRepository.findAll();
	}

	@Override
	public Deplacement findById(Integer id) {
		return deplacementRepository.findById(id).orElse(null);
	}

	@Override
	public Deplacement post(Deplacement deplacement) {
		return deplacementRepository.save(deplacement);
	}

	@Override
	public Deplacement put(Deplacement deplacement) {
		return deplacementRepository.save(deplacement);
	}

	@Override
	public void delete(Integer id) {
		deplacementRepository.deleteById(id);
	}
	
}
