package springboot.services;

import java.util.List;

import springboot.models.Deplacement;
import springboot.models.Infirmiere;
import springboot.models.Patient;

public interface InfirmiereService {

	public List<Infirmiere> findAll();

	public Infirmiere findById(Integer id);

	public Infirmiere post(Infirmiere infirmiere);

	public Infirmiere put(Infirmiere infirmiere);

	public void delete(Integer id);

	public List<Patient> patientById(Integer id);

	public List<Deplacement> deplacementsById(Integer id);

}
