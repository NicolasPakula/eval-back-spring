package springboot.services;

import java.util.List;

import springboot.models.Deplacement;
import springboot.models.Patient;
import springboot.repositories.DeplacementRepository;
import springboot.repositories.PatientRepository;

public class PatientServiceImpl implements PatientService{

	private PatientRepository patientRepository;
	private AdresseService adresseService;
	private DeplacementRepository deplacementRepository;
	
	public PatientServiceImpl(PatientRepository patientRepository,AdresseService adresseService,DeplacementRepository deplacementRepository) {
		this.patientRepository = patientRepository;
		this.adresseService = adresseService;
		this.deplacementRepository = deplacementRepository;
	}
	
	@Override
	public List<Patient> findAll() {
		return patientRepository.findAll();
	}

	@Override
	public Patient findById(Integer id) {
		return patientRepository.findById(id).orElse(null);
	}

	@Override
	public Patient post(Patient patient) {
		patient.setAdresse(adresseService.insertWithExistCheck(patient.getAdresse(), "post"));
		return patientRepository.save(patient);
	}

	@Override
	public Patient put(Patient patient) {
		patient.setAdresse(adresseService.insertWithExistCheck(patient.getAdresse(), "put"));
		return patientRepository.save(patient);
	}

	@Override
	public void delete(Integer id) {
		patientRepository.deleteById(id);
	}

	@Override
	public List<Deplacement> deplacementsById(Integer id) {
		return deplacementRepository.findByPatientId(id);
	}

}
