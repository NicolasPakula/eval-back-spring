package springboot.services;

import java.util.List;

import springboot.models.Adresse;

public interface AdresseService {

	public List<Adresse> findAll();

	public Adresse findById(Integer id);

	public Adresse post(Adresse adresse);

	public Adresse put(Adresse adresse);

	public void delete(Integer id);

	public Adresse insertWithExistCheck(Adresse adresse,String method);
	

}
