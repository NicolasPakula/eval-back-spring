# BACK

Back terminé à 100% contrairement au front malheuresement  

## Lancer l'appli
  
Lancer la bdd  (lien git si jamais celui là ne marche pas https://gitlab.com/NicolasPakula/eval-docker-db)  
cd db_back  
créer un dossier data  
docker compose up  

Nouveau terminal ou revenir à la racine du projet  
mvn install  
mvn spring-boot:run  

Lancer maintenant le front  
Instruction dans le readme à cette adresse https://gitlab.com/NicolasPakula/eval-front-angular  

## API

### Infirmieres

Structure du POST

	{
		"numeroProfessionnel":17018780,
		"nom":"Jean 3",
		"prenom": "Jacque",
		"telPro": "0671921202",
		"telPerso": "0681201292",
		"adresse": {
			"numero": "1",
			"rue": "rue du test",
			"ville": "Lille",
			"cp": "59000"
		}
	}
	
### Patients

structure du POST 

	{
		"sexe":"Homme",
		"nom":"Jean 3",
		"prenom": "Jacque",
		"dateDeNaissance": "2021-09-09",
		"numeroSecuriteSociale": 9891989291,
		"infirmiere":{
			"id":7
		},
		"adresse": {
			"numero": "6",
			"rue": "rue du test",
			"ville": "Lille",
			"cp": "59000"
		}
	}
	
### Deplacements

structure du POST

	{
		"cout": 28.0,
		"date": "2021-09-09T12:12:12",
		"infirmiere": {
			"id": 7
		},
		"patient": {
			"id": 1
		}
	}
	
	
	
### Notes

/patients/{id}/deplacements pour récupérer les déplacements d'un patient
/infirmieres/{id}/deplacements pour récupérer les déplacements d'une infirmiere
/infirmieres/{id}/patients pour récupérer les patients d'une infirmiere

Les gets/getbyid/delete sont basiques
Adresse à un controller parce que why not ça prend 30sec
